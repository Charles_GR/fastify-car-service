const CarArchive = require('./car-archive').CarArchive;

describe('CarArchive', () => {
  let carArchive;

  beforeAll(() => {
    carArchive = new CarArchive('com.example', 'service-one', 'someName', '97a3e3c56aba7121151f056e65421b32', 'test description', 'someData', '1.0.0');
  });

  it('carArchive.groupId should equal correct value', () => {
    expect(carArchive.groupId).toStrictEqual('com.example');
  });

  it('carArchive.artifactId should equal correct value', () => {
    expect(carArchive.artifactId).toStrictEqual('service-one');
  });

  it('carArchive.name should equal correct value', () => {
    expect(carArchive.name).toStrictEqual('someName');
  });

  it('carArchive.hash should equal correct value', () => {
    expect(carArchive.hash).toStrictEqual('97a3e3c56aba7121151f056e65421b32');
  });

  it('carArchive.description should equal correct value', () => {
    expect(carArchive.description).toStrictEqual('test description');
  });

  it('carArchive.content should equal correct value', () => {
    expect(carArchive.content).toStrictEqual('someData');
  });

  it('carArchive.version should equal correct value', () => {
    expect(carArchive.version).toStrictEqual('1.0.0');
  });
});