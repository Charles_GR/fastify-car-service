function CarArchive (groupId, artifactId, name, hash, description, content, version) {
  this.groupId = groupId;
  this.artifactId = artifactId;
  this.name = name;
  this.hash = hash;
  this.description = description;
  this.content = content;
  this.version = version;
}

module.exports = { CarArchive: CarArchive };