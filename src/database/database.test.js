const knex = require('knex')();
const database = require('./database');
const CarArchive = require('../model/car-archive').CarArchive;

describe('Database', () => {
  describe('getCarArchive', () => {
    const id = 3;

    beforeAll(async () => {
      await database.getCarArchive(id);
    });

    afterAll(() => {
      jest.clearAllMocks();
    });

    describe('knex.table', () => {
      it('should be called once', () => {
        expect(knex.table).toHaveBeenCalledTimes(1);
      });

      it('should be called with correct table name', () => {
        expect(knex.table).toHaveBeenCalledWith('CAR_ARCHIVES');
      });
    });

    describe('table.where', () => {
      let table;

      beforeAll(() => {
        table = knex.table();
      });

      it('should be called once', () => {
        expect(table.where).toHaveBeenCalledTimes(1);
      });

      it('should be called with correct column name', () => {
        expect(table.where).toHaveBeenCalledWith('id', expect.anything());
      });

      it('should be called with correct column value', () => {
        expect(table.where).toHaveBeenCalledWith(expect.anything(), id);
      });
    });

    it('where.select should be called once', () => {
      const where = knex.table().where();
      expect(where.select).toHaveBeenCalledTimes(1);
    });

    it('select.first should be called once', () => {
      const select = knex.table().where().select();
      expect(select.first).toHaveBeenCalledTimes(1);
    });
  });

  describe('addCarArchive', () => {
    const carArchive = new CarArchive('com.example', 'service-one', 'someFile', '97a3e3c56aba7121151f056e65421b32', 'test description', 'someData', '1.0.0');

    beforeAll(async () => {
      await database.addCarArchive(carArchive);
    });

    afterAll(() => {
      jest.clearAllMocks();
    });

    describe('knex.table', () => {
      it('should be called once', () => {
        expect(knex.table).toHaveBeenCalledTimes(1);
      });

      it('should be called with correct table name', () => {
        expect(knex.table).toHaveBeenCalledWith('CAR_ARCHIVES');
      });
    });

    describe('table.insert', () => {
      let table;

      beforeAll(() => {
        table = knex.table();
      });

      it('should be called once', () => {
        expect(table.insert).toHaveBeenCalledTimes(1);
      });

      it('should be called with correct CAR archive', () => {
        expect(table.insert).toHaveBeenCalledWith({
          GROUP_ID: carArchive.groupId,
          ARTIFACT_ID: carArchive.artifactId,
          NAME: carArchive.name,
          HASH: carArchive.hash,
          DESCRIPTION: carArchive.description,
          VERSION: carArchive.version
        });
      });
    });

    describe('insert.returning', () => {
      let insert;

      beforeAll(() => {
        insert = knex.table().insert();
      });

      it('should be called once', () => {
        expect(insert.returning).toHaveBeenCalledTimes(1);
      });

      it('should be called with correct column name', () => {
        expect(insert.returning).toHaveBeenCalledWith('id');
      });
    });

    it('carArchive.id should be set to correct id', () => {
      const expected = knex.table().insert().returning();
      expect(carArchive.id).toStrictEqual(expected);
    });
  });
});