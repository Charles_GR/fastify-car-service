const databaseConfig = require('./dbConfig');
const knex = require('knex')(databaseConfig);

async function getCarArchive(id) {
  try {
    const carArchive = await knex.table('CAR_ARCHIVES')
      .where('id', id)
      .select({ name: 'name' })
      .first();
    return { success: true, object: carArchive };
  } catch(error) {
    return { success: false, error: error } ;
  }
}

async function addCarArchive(carArchive) {
  try {
    carArchive.id = await knex.table('CAR_ARCHIVES')
      .insert({
        GROUP_ID: carArchive.groupId,
        ARTIFACT_ID: carArchive.artifactId,
        NAME: carArchive.name,
        HASH: carArchive.hash,
        DESCRIPTION: carArchive.description,
        VERSION: carArchive.version
    }).returning('id');
    return { success: true, object: carArchive };
  } catch (error) {
    return { success: false, error: error } ;
  }
}

module.exports = { getCarArchive, addCarArchive };