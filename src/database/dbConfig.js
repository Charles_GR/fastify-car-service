module.exports = {
  client: "mssql",
  connection: {
    host: process.env.DB_HOST || 'localhost',
    port: process.env.DB_PORT || 1433,
    user: process.env.DB_USER || 'sa',
    password: process.env.DB_PASSWORD || 'Passw0rd',
    database: process.env.DB_NAME || 'connections',
    options: {
      enableArithAbort: true
    }
  }
};
