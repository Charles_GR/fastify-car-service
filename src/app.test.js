const fileSystem = require('fs');
const FormData = require('form-data');
const HttpStatus = require('http-status-codes');
const fastify = require('./app');
const database = require('./database/database');
const CarArchive = require('./model/car-archive').CarArchive;

jest.mock('./database/database');

describe('App Integration', () => {
  const carArchive = { name: 'car.car' };

  afterAll(async () => {
    await fastify.close();
  });

  describe('Import', () => {
    let response;

    beforeAll(async () => {
      database.addCarArchive.mockReturnValue({ success: true, object: carArchive });

      const form = new FormData();
      const fileStream = fileSystem.createReadStream('src/resources/car.car');
      form.append('file', fileStream);

      response = await fastify.inject({
        url: 'v1/cararchives',
        method: 'POST',
        payload: form,
        headers: form.getHeaders()
      });
    });

    afterAll(() => {
      database.addCarArchive.mockReset();
    });

    describe('database.addCarArchive', () => {
      it('should be called once', () => {
        expect(database.addCarArchive).toHaveBeenCalledTimes(1);
      });

      it('should be called with correct CAR archive', () => {
        expect(database.addCarArchive).toHaveBeenCalledWith(
          new CarArchive('com.example', 'service-one', 'car.car', '97a3e3c56aba7121151f056e65421b32', 'test description', null, '1.0.0'));
      });
    });

    it('response.statusCode should equal CREATED', () => {
      expect(response.statusCode).toStrictEqual(HttpStatus.CREATED);
    });

    it('response.payload should equal correct value', () => {
      const payload = JSON.parse(response.payload);
      expect(payload).toStrictEqual({ carArchive });
    });
  });

  describe('Export', () => {
    const id = '3';
    let response;

    beforeAll(async () => {
      database.getCarArchive.mockReturnValue({ success: true, object: carArchive });
      await fileSystem.writeFileSync('./cache/car.car', 'data');

      response = await fastify.inject({
        url: `v1/cararchives/${id}`,
        method: 'GET'
      });
    });

    afterAll(async () => {
      database.getCarArchive.mockReset();
      await fileSystem.unlinkSync('./cache/car.car');
    });

    describe('database.getCarArchive', () => {
      it('should be called once', () => {
        expect(database.getCarArchive).toHaveBeenCalledTimes(1);
      });

      it('should be called with correct id', () => {
        expect(database.getCarArchive).toHaveBeenCalledWith(id);
      });
    });

    it('response.statusCode should equal OK', () => {
      expect(response.statusCode).toStrictEqual(HttpStatus.OK);
    });

    it('response.payload should equal correct value', () => {
      expect(response.payload).toStrictEqual('data');
    });
  });
});