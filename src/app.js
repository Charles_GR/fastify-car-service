const fastify = require('fastify')();

fastify.register(require('fastify-file-upload'));
fastify.register(require('./endpoints/import/import'));
fastify.register(require('./endpoints/export/export'));

fastify.listen(8080, '0.0.0.0',function (error, address) {
    if (error) {
        console.error(error);
        process.exit(1);
    }
    console.info(`server listening on ${address}`);
});

module.exports = fastify;
