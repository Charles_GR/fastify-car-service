const databaseConfig = require('../database/dbConfig');
const knex = require('knex')(databaseConfig);
const fileSystem = require('fs');
const fastify = require('../app');
const FormData = require('form-data');
const HttpStatus = require('http-status-codes');

jest.unmock('knex');

describe('App E2E', () => {
  afterAll(async () => {
    await fastify.close();
  });

  describe('Import', () => {
    let response;

    beforeAll(async () => {
      await knex.table('CAR_ARCHIVES').delete();

      const form = new FormData();
      const fileStream = fileSystem.createReadStream('src/resources/car.car');
      form.append('file', fileStream);

      response = await fastify.inject({
        url: 'v1/cararchives',
        method: 'POST',
        payload: form,
        headers: form.getHeaders()
      });
    });

    afterAll(async () => {
      await knex.table('CAR_ARCHIVES').delete();
    });

    it('response.statusCode should equal CREATED', () => {
      expect(response.statusCode).toStrictEqual(HttpStatus.CREATED);
    });

    it('response.payload should equal correct value', () => {
      const payload = JSON.parse(response.payload);
      expect(payload).toStrictEqual({
        carArchive: {
          id: [ expect.any(Number) ],
          groupId: 'com.example',
          artifactId: 'service-one',
          name: 'car.car',
          hash: '97a3e3c56aba7121151f056e65421b32',
          description: 'test description',
          content: null,
          version: '1.0.0'
        }
      });
    });
  });

  describe('Export', () => {
    const data = 'someData';
    let response;

    beforeAll(async () => {
      await fileSystem.writeFileSync('./cache/car.car', data);

      await knex.table('CAR_ARCHIVES').delete();
      const id = await knex.table('CAR_ARCHIVES')
        .insert({
          GROUP_ID: 'com.example',
          ARTIFACT_ID: 'service-one',
          NAME: 'car.car',
          HASH: '97a3e3c56aba7121151f056e65421b32',
          DESCRIPTION: 'test description',
          VERSION: '1.0.0'
        }).returning('id');

      response = await fastify.inject({
        url: `v1/cararchives/${id}`,
        method: 'GET'
      });
    });

    afterAll(async () => {
      await fileSystem.unlinkSync('./cache/car.car');
      await knex.table('CAR_ARCHIVES').delete();
    });

    it('response.statusCode should equal OK', () => {
      expect(response.statusCode).toStrictEqual(HttpStatus.OK);
    });

    it('response.payload should equal correct value', () => {
      expect(response.payload).toStrictEqual(data);
    });
  });
});