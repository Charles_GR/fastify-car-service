const fileSystem = require('fs');
const HttpStatus = require('http-status-codes');
const fastify = require('fastify');
const importCarArchive = require('./import');
const database = require('../../database/database');
const CarArchive = require('../../model/car-archive').CarArchive;

jest.mock('fs');
jest.mock('fastify');
jest.mock('../../database/database');

describe('importCarArchive', () => {
  const options = { option: 'Value' };

  beforeAll(async () => {
    fastify.post = jest.fn();
    await importCarArchive(fastify, options);
  });

  describe('fastify.post', () => {
    it('should be called once', () => {
      expect(fastify.post).toHaveBeenCalledTimes(1);
    });

    it('should be called with correct URL', () => {
      expect(fastify.post).toHaveBeenCalledWith('/v1/cararchives', expect.anything(), expect.anything());
    });

    it('should be called with correct options', () => {
      expect(fastify.post).toHaveBeenCalledWith(expect.anything(), options, expect.anything());
    });

    it('should be called with a callback function', () => {
      expect(fastify.post).toHaveBeenCalledWith(expect.anything(), expect.anything(), expect.any(Function))
    });
  });

  describe('fastify.post callback function', () => {
    const file = {
      name: 'someFile',
      mv: jest.fn()
    };
    const request = {
      raw: {
        files: {
          file: file
        }
      }
    };
    const codeResponse = {
      send: jest.fn()
    };
    const response = {
      code: jest.fn().mockReturnValue(codeResponse)
    };

    describe('when database operation is success', () => {
      const result = { success: true, id: 3 };

      beforeAll(async () => {
        database.addCarArchive.mockReturnValue(result);
      });

      describe('when fileSystem.existsSync returns true', () => {
        beforeAll(async () => {
          fileSystem.existsSync.mockReturnValue(true);
          await fastify.post.mock.calls[0][2](request, response);
        });

        afterAll(() => {
          tearDown();
        });

        describe('database.addCarArchive', () => {
          it('should be called once', () => {
            expect(database.addCarArchive).toHaveBeenCalledTimes(1);
          });

          it('should be called with correct CAR archive', () => {
            expect(database.addCarArchive).toHaveBeenCalledWith(
              new CarArchive('com.example', 'service-one', file.name, '97a3e3c56aba7121151f056e65421b32', 'test description', null, '1.0.0'));
          });
        });

        describe('response.code', () => {
          it('should be called once', () => {
            expect(response.code).toHaveBeenCalledTimes(1);
          });

          it('should be called with correct status code', () => {
            expect(response.code).toHaveBeenCalledWith(HttpStatus.CREATED);
          });
        });

        describe('codeResponse.send', () => {
          it('should be called once', () => {
            expect(codeResponse.send).toHaveBeenCalledTimes(1);
          });

          it('should be called with correct body', () => {
            expect(codeResponse.send).toHaveBeenCalledWith({ carArchive: result.object });
          });
        });
      });

      describe('when fileSystem.existsSync returns false', () => {
        beforeAll(async () => {
          fileSystem.existsSync.mockReturnValue(false);
          await fastify.post.mock.calls[0][2](request, response);
        });

        afterAll(() => {
          tearDown();
        });

        describe('database.addCarArchive', () => {
          it('should be called once', () => {
            expect(database.addCarArchive).toHaveBeenCalledTimes(1);
          });

          it('should be called with correct CAR archive', () => {
            expect(database.addCarArchive).toHaveBeenCalledWith(
              new CarArchive('com.example', 'service-one', file.name, '97a3e3c56aba7121151f056e65421b32', 'test description', null, '1.0.0'));
          });
        });

        describe('file.mv', () => {
          it('should be called once', () => {
            expect(file.mv).toHaveBeenCalledTimes(1);
          });

          it('should be called with correct file path', () => {
            expect(file.mv).toHaveBeenCalledWith(`./cache/${file.name}`, expect.anything());
          });

          it('should be called with a callback function', () => {
            expect(file.mv).toHaveBeenCalledWith(expect.anything(), expect.any(Function));
          });
        });

        describe('file.mv callback function', () => {
          beforeAll(async () => {
            await file.mv.mock.calls[0][1]();
          });

          describe('response.code', () => {
            it('should be called once', () => {
              expect(response.code).toHaveBeenCalledTimes(1);
            });

            it('should be called with correct status code', () => {
              expect(response.code).toHaveBeenCalledWith(HttpStatus.CREATED);
            });
          });

          describe('codeResponse.send', () => {
            it('should be called once', () => {
              expect(codeResponse.send).toHaveBeenCalledTimes(1);
            });

            it('should be called with correct body', () => {
              expect(codeResponse.send).toHaveBeenCalledWith({carArchive: result.object});
            });
          });
        });
      });
    });

    describe('when database operation is failure', () => {
      const result = { success: false, error: 'someError' };

      beforeAll(async () => {
        database.addCarArchive.mockReturnValue(result);
      });

      describe('when fileSystem.existsSync returns true', () => {
        beforeAll(async () => {
          fileSystem.existsSync.mockReturnValue(true);
          await fastify.post.mock.calls[0][2](request, response);
        });

        afterAll(() => {
          tearDown();
        });

        describe('database.addCarArchive', () => {
          it('should be called once', () => {
            expect(database.addCarArchive).toHaveBeenCalledTimes(1);
          });

          it('should be called with correct CAR archive', () => {
            expect(database.addCarArchive).toHaveBeenCalledWith(
              new CarArchive('com.example', 'service-one', file.name, '97a3e3c56aba7121151f056e65421b32', 'test description', null, '1.0.0'));
          });
        });

        describe('response.code', () => {
          it('should be called once', () => {
            expect(response.code).toHaveBeenCalledTimes(1);
          });

          it('should be called with correct status code', () => {
            expect(response.code).toHaveBeenCalledWith(HttpStatus.INTERNAL_SERVER_ERROR);
          });
        });

        describe('codeResponse.send', () => {
          it('should be called once', () => {
            expect(codeResponse.send).toHaveBeenCalledTimes(1);
          });

          it('should be called with correct body', () => {
            expect(codeResponse.send).toHaveBeenCalledWith(result.error);
          });
        });
      });

      describe('when fileSystem.existsSync returns false', () => {
        beforeAll(async () => {
          fileSystem.existsSync.mockReturnValue(false);
          await fastify.post.mock.calls[0][2](request, response);
        });

        afterAll(() => {
          tearDown();
        });

        describe('database.addCarArchive', () => {
          it('should be called once', () => {
            expect(database.addCarArchive).toHaveBeenCalledTimes(1);
          });

          it('should be called with correct CAR archive', () => {
            expect(database.addCarArchive).toHaveBeenCalledWith(
              new CarArchive('com.example', 'service-one', file.name, '97a3e3c56aba7121151f056e65421b32', 'test description', null, '1.0.0'));
          });
        });

        describe('file.mv', () => {
          it('should be called once', () => {
            expect(file.mv).toHaveBeenCalledTimes(1);
          });

          it('should be called with correct file path', () => {
            expect(file.mv).toHaveBeenCalledWith(`./cache/${file.name}`, expect.anything());
          });

          it('should be called with a callback function', () => {
            expect(file.mv).toHaveBeenCalledWith(expect.anything(), expect.any(Function));
          });
        });

        describe('file.mv callback function', () => {
          beforeAll(async () => {
            await file.mv.mock.calls[0][1]();
          });

          describe('response.code', () => {
            it('should be called once', () => {
              expect(response.code).toHaveBeenCalledTimes(1);
            });

            it('should be called with correct status code', () => {
              expect(response.code).toHaveBeenCalledWith(HttpStatus.INTERNAL_SERVER_ERROR);
            });
          });

          describe('codeResponse.send', () => {
            it('should be called once', () => {
              expect(codeResponse.send).toHaveBeenCalledTimes(1);
            });

            it('should be called with correct body', () => {
              expect(codeResponse.send).toHaveBeenCalledWith(result.error);
            });
          });
        });
      });
    });

    function tearDown() {
      fileSystem.existsSync.mockReset();
      database.addCarArchive.mockClear();
      file.mv.mockClear();
      response.code.mockClear();
      codeResponse.send.mockClear();
    }
  });
});
