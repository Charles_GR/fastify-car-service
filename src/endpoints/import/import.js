const fileSystem = require('fs');
const HttpStatus = require('http-status-codes');
const database = require('../../database/database');
const CarArchive = require('../../model/car-archive').CarArchive;

async function importCarArchive(fastify, options) {
  fastify.post('/v1/cararchives', options, async (request, response) => {
    const file = request.raw.files.file;
    const filePath = `./cache/${file.name}`;
    const carArchive = new CarArchive('com.example', 'service-one', file.name, '97a3e3c56aba7121151f056e65421b32', 'test description', null, '1.0.0');
    const result = await database.addCarArchive(carArchive);

    if (!fileSystem.existsSync(filePath)) {
      file.mv(`./cache/${file.name}`, () => {
        console.log(`./cache/${file.name} saved successfully`);
        result.success ? response.code(HttpStatus.CREATED).send({ carArchive: result.object }) : response.code(HttpStatus.INTERNAL_SERVER_ERROR).send(result.error);
      });
    } else {
      result.success ? response.code(HttpStatus.CREATED).send({ carArchive: result.object }) : response.code(HttpStatus.INTERNAL_SERVER_ERROR).send(result.error);
    }
  });
}

module.exports = importCarArchive;
