const fileSystem = require('fs');
const HttpStatus = require('http-status-codes');
const database = require('../../database/database');

async function exportCarArchive(fastify, options) {
  fastify.get('/v1/cararchives/:id', options, async (request, response) => {
    const result = await database.getCarArchive(request.params.id);

    if (!result.success) {
      return response.code(HttpStatus.INTERNAL_SERVER_ERROR).send(result.error);
    }
    if (!result.object) {
      return response.code(HttpStatus.NOT_FOUND).send();
    }

    const fileName = result.object.name;
    if(fileSystem.existsSync(`./cache/${fileName}`)) {
      response
        .code(HttpStatus.OK)
        .type('application/octet-stream')
        .header('Content-Disposition', `attachment; filename="${fileName}"`)
        .send(fileSystem.createReadStream(`./cache/${fileName}`));
    } else {
      console.log('The file does not exist.');
    }
  });
}

module.exports = exportCarArchive;
