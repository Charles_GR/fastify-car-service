const fileSystem = require('fs');
const HttpStatus = require('http-status-codes');
const fastify = require('fastify');
const exportCarArchive = require('./export');
const database = require('../../database/database');

jest.mock('fs');
jest.mock('fastify');
jest.mock('../../database/database');

describe('exportCarArchive', () => {
  const options = { option: 'Value' };

  beforeAll(async () => {
    fastify.get = jest.fn();
    await exportCarArchive(fastify, options);
  });

  describe('fastify.get', () => {
    it('should be called once', () => {
      expect(fastify.get).toHaveBeenCalledTimes(1);
    });

    it('should be called with correct URL', () => {
      expect(fastify.get).toHaveBeenCalledWith('/v1/cararchives/:id', expect.anything(), expect.anything());
    });

    it('should be called with correct options', () => {
      expect(fastify.get).toHaveBeenCalledWith(expect.anything(), options, expect.anything());
    });

    it('should be called with a callback function', () => {
      expect(fastify.get).toHaveBeenCalledWith(expect.anything(), expect.anything(), expect.any(Function))
    });
  });

  describe('fastify.get callback function', () => {
    let callback;
    const result = {};
    const request = {
      params: {
        id: 3
      }
    };

    beforeAll(() => {
      database.getCarArchive.mockReturnValue(result);
      callback = fastify.get.mock.calls[0][2];
    });

    afterAll(() => {
      database.getCarArchive.mockReset();
    });

    describe('when database operation is success', () => {
      beforeAll(() => {
        result.success = true;
      });

      afterAll(() => {
        delete result.success;
      });

      describe('when CAR archive is found', () => {
        const send = jest.fn();
        const okResponse = { send: send };
        const typeResponse = { header: jest.fn().mockReturnValue(okResponse) };
        const codeResponse = { type: jest.fn().mockReturnValue(typeResponse), send: send };
        const response = { code: jest.fn().mockReturnValue(codeResponse) };
        const carArchive = { name: 'car.car' };
        const stream = 'aStream';

        beforeAll(() => {
          result.object = carArchive;
          fileSystem.createReadStream.mockReturnValue(stream);
        });

        afterAll(() => {
          delete result.object;
        });

        describe('when fileSystem.existsSync returns true', () => {
          beforeAll(async () => {
            fileSystem.existsSync.mockReturnValue(true);
            await callback(request, response);
          });

          afterAll(() => {
            jest.clearAllMocks();
          });

          describe('fileSystem.existsSync', () => {
            it('should be called once', () => {
              expect(fileSystem.existsSync).toHaveBeenCalledTimes(1);
            });

            it('should be called with correct path', () => {
              expect(fileSystem.existsSync).toHaveBeenCalledWith(`./cache/${carArchive.name}`);
            });
          });

          describe('response.code', () => {
            it('should be called once', () => {
              expect(response.code).toHaveBeenCalledTimes(1);
            });

            it('should be called with correct status', () => {
              expect(response.code).toHaveBeenCalledWith(HttpStatus.OK);
            });
          });

          describe('codeResponse.type', () => {
            it('should be called once', () => {
              expect(codeResponse.type).toHaveBeenCalledTimes(1);
            });

            it('should be called with correct value', () => {
              expect(codeResponse.type).toHaveBeenCalledWith('application/octet-stream');
            });
          });

          describe('typeResponse.header', () => {
            it('should be called once', () => {
              expect(typeResponse.header).toHaveBeenCalledTimes(1);
            });

            it('should be called with correct key', () => {
              expect(typeResponse.header).toHaveBeenCalledWith('Content-Disposition', expect.anything());
            });

            it('should be called with correct value', () => {
              expect(typeResponse.header).toHaveBeenCalledWith(expect.anything(), 'attachment; filename="car.car"');
            });
          });

          describe('okResponse.send', () => {
            it('should be called once', () => {
              expect(okResponse.send).toHaveBeenCalledTimes(1);
            });

            it('should be called with correct value', () => {
              expect(okResponse.send).toHaveBeenCalledWith(stream);
            });
          });
        });

        describe('when fileSystem.existsSync returns false', () => {
          beforeAll(async () => {
            fileSystem.existsSync.mockReturnValue(false);
            await callback(request, response);
          });

          afterAll(() => {
            jest.clearAllMocks();
          });

          describe('fileSystem.existsSync', () => {
            it('should be called once', () => {
              expect(fileSystem.existsSync).toHaveBeenCalledTimes(1);
            });

            it('should be called with correct path', () => {
              expect(fileSystem.existsSync).toHaveBeenCalledWith(`./cache/${carArchive.name}`);
            });
          });

          it('response.code should not be called', () => {
            expect(response.code).not.toHaveBeenCalled();
          });

          it('codeResponse.type should not be called', () => {
            expect(codeResponse.type).not.toHaveBeenCalled();
          });

          it('typeResponse.header should not be called', () => {
            expect(typeResponse.header).not.toHaveBeenCalled();
          });

          it('okResponse.send should not be called', () => {
            expect(okResponse.send).not.toHaveBeenCalled();
          });

          it('fileSystem.createReadStream should not be called', () => {
            expect(fileSystem.createReadStream).not.toHaveBeenCalled();
          });
        });
      });

      describe('when CAR archive is not found', () => {
        const notFoundResponse = { send: jest.fn() };
        const response = { code: jest.fn().mockReturnValue(notFoundResponse) };

        beforeAll(async () => {
          result.object = undefined;
          await callback(request, response);
        });

        afterAll(() => {
          delete result.object;
          jest.clearAllMocks();
        });

        describe('response.code', () => {
          it('should be called once', () => {
            expect(response.code).toHaveBeenCalledTimes(1);
          });

          it('should be called with correct code', () => {
            expect(response.code).toHaveBeenCalledWith(HttpStatus.NOT_FOUND);
          });
        });

        it('notFoundResponse.send should be called once', () => {
          expect(notFoundResponse.send).toHaveBeenCalledTimes(1);
        });

        it('fileSystem.createReadStream should not be called', () => {
          expect(fileSystem.createReadStream).not.toHaveBeenCalled();
        });
      });
    });

    describe('when database operation is failure', () => {
      const serverErrorResponse = { send: jest.fn() };
      const response = { code: jest.fn().mockReturnValue(serverErrorResponse) };

      beforeAll(async () => {
        result.success = false;
        result.error = new Error();
        await callback(request, response);
      });

      afterAll(() => {
        delete result.success;
        delete result.error;
        jest.clearAllMocks();
      });

      describe('response.code', () => {
        it('should be called once', () => {
          expect(response.code).toHaveBeenCalledTimes(1);
        });

        it('should be called with correct code', () => {
          expect(response.code).toHaveBeenCalledWith(HttpStatus.INTERNAL_SERVER_ERROR);
        });
      });

      describe('serverErrorResponse.send', () => {
        it('should be called once', () => {
          expect(serverErrorResponse.send).toHaveBeenCalledTimes(1);
        });

        it('should be called with correct error', () => {
          expect(serverErrorResponse.send).toHaveBeenCalledWith(result.error);
        });
      });
    });
  });
});