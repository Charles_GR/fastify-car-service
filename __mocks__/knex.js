const first = jest.fn();
const select = jest.fn().mockReturnValue({ first });
const where = jest.fn().mockReturnValue({ select });
const returning = jest.fn().mockReturnValue(64);
const insert = jest.fn().mockReturnValue({ returning });
const table = jest.fn().mockReturnValue({ where, insert });

module.exports = () => ({ table });