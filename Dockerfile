FROM node:stretch-slim
WORKDIR /usr/src/app

COPY . .
RUN mkdir -p /cache
RUN chmod ug+rwx /cache
RUN npm install
RUN npm install pm2 -g

EXPOSE 8080
CMD pm2-runtime start src/app.js -i max
